package list.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import list.entities.TaskList;

@Repository
public interface TaskListRepository extends CrudRepository<TaskList, Integer> {}