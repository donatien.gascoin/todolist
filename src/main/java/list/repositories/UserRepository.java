package list.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import list.entities.User;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByLogin(String login);

    Optional<User> findByLoginAndMail(String login, String mail);
}