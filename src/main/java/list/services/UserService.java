package list.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import list.dto.request.CheckUserDto;
import list.dto.request.ResetPasswordDto;
import list.dto.request.SignUpDto;
import list.dto.request.UserDto;
import list.dto.response.TaskListShort;
import list.dto.response.UserResponse;
import list.entities.Task;
import list.entities.TaskList;
import list.entities.User;
import list.feign.dto.CaptchaResponse;
import list.feign.services.ReCaptchaClient;
import list.repositories.UserRepository;
import list.security.UserPrincipal;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ReCaptchaClient reCaptchaClient;

    @Autowired
    private Environment env;

    public User getUser(Integer id) throws TodoListException {
        try {
            Optional<User> optional = userRepository.findById(id);
            if (optional.isPresent()) {
                User u = optional.get();
                u.setTaskLists(removeDuplicates(u.getTaskLists()));
                // u.getTaskLists().forEach(f -> f.setTasks(new ArrayList<>()));
                return u;
            } else {
                throw new TodoListException(ErrorCode.UNKNOWN_USER);
            }
        } catch (IllegalArgumentException e) {
            log.error("Impossible to get user with id {} ", id, e);
            throw new TodoListException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login)
            .orElseThrow(() -> new UsernameNotFoundException(ErrorCode.UNKNOWN_USER.name()));
        return UserPrincipal.create(user);
    }

    public User editUser(UserDto dto, int id) throws TodoListException {
        log.info("Edit user [ {} ] ", id);
        try {

            User user = getUser(id);

            user.setMail(dto.getMail());
            user.setName(dto.getName());
            user.setPassword(dto.getPassword());

            user = userRepository.save(user);

            log.info("Edit user [ {} ] - Edited", id);
            return user;
        } catch (TodoListException e) {
            throw e;
        } catch (Exception e) {
            log.error("Edit user [ {} ] - Unknown error", id, e);
            throw new TodoListException(ErrorCode.INTERNAL_ERROR);
        }
    }

    public void deleteUser(UserPrincipal currentUser, int id) throws TodoListException {
        log.info("Delete user [ {} ]", id);
        try {
            if (!userRepository.existsById(id)) {
                log.info("Delete user [ {} ] - User not found", id);
                throw new TodoListException(ErrorCode.UNKNOWN_USER);
            }

            userRepository.deleteById(id);
            log.info("Delete user [ {} ] - User deleted");
        } catch (TodoListException t) {
            throw t;
        } catch (Exception e) {
            log.error("Delete user [ {} ] - Unknown error", id, e);
            throw new TodoListException(ErrorCode.INTERNAL_ERROR);
        }
    }

    public User signUpUser(SignUpDto userToCreate) throws TodoListException, IllegalArgumentException {
        log.info("Sign up user [ {} ]", userToCreate.getLogin());
        if (userRepository.findByLogin(userToCreate.getLogin()).isPresent()) {
            log.error("Sign up user [ {} ] - Login already taken", userToCreate.getLogin());
            throw new TodoListException(ErrorCode.LOGIN_ALREADY_TAKEN);
        }

        // Creating user's account
        User user = new User(userToCreate.getLogin(),
            userToCreate.getMail(),
            userToCreate.getName(),
            passwordEncoder.encode(userToCreate.getPassword()));
        user.setShowDescription(true);
        User result = userRepository.save(user);
        log.info("Sign up user [ {} ] - Added", userToCreate.getLogin());

        return result;
    }

    public void removeTaskList(User u, TaskList tl) {

        u.getTaskLists().remove(tl);
        userRepository.save(u);
    }

    public boolean isListBelongToUser(int userId, int listId) throws TodoListException {
        User u = userRepository.findById(userId).orElseThrow(() -> new TodoListException(ErrorCode.UNKNOWN_USER));

        for (TaskList tl : u.getTaskLists()) {
            if (tl.getId() == listId) {
                return true;
            }
        }
        return false;
    }

    private List<TaskList> removeDuplicates(List<TaskList> l) {
        // ... the list is already populated
        Set<TaskList> s = new TreeSet<TaskList>((o1, o2) -> {
            if (o1.getId() <= o2.getId()) {
                return o1.getId() == o2.getId() ? 0 : -1;
            }
            return 1;
        });
        s.addAll(l);
        return new ArrayList<>(s);
    }

    public UserResponse getShortUser(int userId) throws TodoListException {
        User u = getUser(userId);
        UserResponse ur = new UserResponse();
        ur.setId(u.getId());
        ur.setLogin(u.getLogin());
        ur.setMail(u.getMail());
        ur.setName(u.getName());
        ur.setShowDescription(u.getShowDescription());
        List<TaskListShort> taskListShorts = new ArrayList<>();
        if (!CollectionUtils.isEmpty(u.getTaskLists())) {
            for (TaskList tl : u.getTaskLists()) {
                TaskListShort tls = new TaskListShort();
                tls.setId(tl.getId());
                tls.setColor(tl.getColor());
                tls.setTitle(tl.getTitle());
                tls.setNbTask(tl.getTasks().size());
                int nb = 0;
                if (!CollectionUtils.isEmpty(tl.getTasks())) {
                    for (Task t : tl.getTasks()) {
                        nb += t.isChecked() ? 1 : 0;
                    }
                }
                tls.setNbCompletedTask(nb);
                taskListShorts.add(tls);
            }
        }
        ur.setTaskLists(taskListShorts);

        return ur;
    }

    public void updateDescription(int userId, Boolean showDescription) throws TodoListException {
        User user = getUser(userId);
        user.setShowDescription(showDescription);
        userRepository.save(user);
    }

    public void isUserByLoginAndMailExist(CheckUserDto userDto) throws TodoListException {
        Optional<User> user = userRepository.findByLoginAndMail(userDto.getLogin(), userDto.getMail());
        if (!user.isPresent()) {
            throw new TodoListException(ErrorCode.UNKNOWN_USER);
        }

    }

    public void resetPassword(ResetPasswordDto dto) throws TodoListException {
        CaptchaResponse response = reCaptchaClient
            .verify(env.getProperty("google.reCaptcha.secret"), dto.getToken(), "");
        if (!response.isSuccess()) {
            log.error("Error when reaching google reCaptcha: {}", response.getErrorCodes());
            throw new TodoListException(ErrorCode.WRONG_CAPTCHA);
        }
        Optional<User> opt = userRepository.findByLoginAndMail(dto.getLogin(), dto.getMail());
        if (!opt.isPresent()) {
            throw new TodoListException(ErrorCode.UNKNOWN_USER);
        }

        User user = opt.get();

        user.setPassword(passwordEncoder.encode(dto.getPassword()));

        userRepository.save(user);
    }
}
