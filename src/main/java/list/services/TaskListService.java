package list.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import list.dto.request.TaskListDto;
import list.entities.Task;
import list.entities.TaskList;
import list.entities.User;
import list.repositories.TaskListRepository;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;

@Slf4j
@Service
public class TaskListService {

    @Autowired
    private TaskListRepository taskListRepository;

    @Autowired
    private UserService userService;

    public TaskList getTaskList(int listId) throws TodoListException {
        return taskListRepository.findById(listId).orElseThrow(() -> new TodoListException(ErrorCode.UNKNOWN_TASK_LIST));
    }

    public TaskList addTaskList(int userId, TaskListDto dto) throws TodoListException {
        User user = userService.getUser(userId);
        TaskList taskList = new TaskList(dto.getTitle(), dto.getColor(), user);
        return taskListRepository.save(taskList);
    }

    public void removeTaskList(int userId, int listId) throws TodoListException {
        log.error("Remove task list [ {} ] for user [ {} ]", listId, userId);
        if (!taskListRepository.existsById(listId)) {
            log.error("Remove task list [ {} ] for user [ {} ] - List not found", listId, userId);
            throw new TodoListException(ErrorCode.UNKNOWN_TASK_LIST);
        }

        User u = userService.getUser(userId);
        for (TaskList tl : u.getTaskLists()) {
            if (tl.getId() == listId) {
                // Delete list only if the user is the owner
                userService.removeTaskList(u, tl);
                taskListRepository.delete(tl);
                return;
            }
        }
        log.warn("Remove task list [ {} ] for user [ {} ] - List does not belong to the user", listId, userId);
        throw new TodoListException(ErrorCode.INSUFFICIENT_PERMISSION);
    }

    public TaskList editTaskList(int userId, TaskListDto dto) throws TodoListException {
        log.info("Edit task list [ {} ] for user [ {} ]", dto.getId(), userId);

        if (!taskListRepository.existsById(dto.getId())) {
            log.error("Edit task list [ {} ] for user [ {} ] - List not found", dto.getId(), userId);
            throw new TodoListException(ErrorCode.UNKNOWN_TASK_LIST);
        }
        try {

            TaskList taskList = taskListRepository.findById(dto.getId()).orElseThrow(() -> new TodoListException(ErrorCode.UNKNOWN_TASK_LIST));
            taskList.setTitle(dto.getTitle());
            taskList.setColor(dto.getColor());

            taskList = taskListRepository.save(taskList);

            log.info("Edit task list [ {} ] for user [ {} ] - Edited", dto.getId(), userId);
            return taskList;
        } catch (TodoListException e) {
            throw e;
        } catch (Exception e) {
            log.error("Edit task list [ {} ] for user [ {} ] - Unknown error", dto.getId(), userId, e);
            throw new TodoListException(ErrorCode.INTERNAL_ERROR);
        }
    }

    public void removeTask(TaskList tl, Task t) {
        tl.getTasks().remove(t);
        taskListRepository.save(tl);
    }

    public TaskList getTaskListById(int userId, int id) throws TodoListException{

        TaskList taskList = getTaskList(id);

        if(taskList.getUser().getId() != userId) {
            throw new TodoListException(ErrorCode.INSUFFICIENT_PERMISSION);
        }

        return taskList;
    }
}
