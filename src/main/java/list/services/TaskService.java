package list.services;

import list.dto.request.TaskDto;
import list.entities.Task;
import list.entities.TaskList;
import list.repositories.TaskRepository;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Slf4j
@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskListService taskListService;

    @Autowired
    private UserService userService;

    public Task addTask(int userId, TaskDto dto) throws TodoListException {
        log.info("Add task to list [ {} ]", dto.getListId());

        TaskList taskList = taskListService.getTaskList(dto.getListId());

        if(taskList.getUser().getId() != userId) {
            log.error("Add task to list [ {} ] -  List does not belong to the user", dto.getListId());
            throw new TodoListException(ErrorCode.INSUFFICIENT_PERMISSION);
        }

        Task task = new Task(dto.getTitle(), dto.getDescription(), getCurrentDate(), dto.isChecked(), taskList);
        task = taskRepository.save(task);
        log.info("Add task to list [ {} ] - Added", dto.getListId());
        return task;
    }

    public void removeTask(int userId, int listId, int taskId) throws TodoListException {
        log.info("Remove task to list [ {} ]", listId);

        if (!taskRepository.existsById(taskId)) {
            log.error("Remove task to list [ {} ] -  Task not found", listId);
            throw new TodoListException(ErrorCode.UNKNOWN_TASK);
        }

        TaskList tl = taskListService.getTaskList(listId);

        if(tl.getUser().getId() != userId) {
            log.error("Remove task to list [ {} ] -  List does not belong to the user", listId);
            throw new TodoListException(ErrorCode.INSUFFICIENT_PERMISSION);
        }

        for(Task t: tl.getTasks()){
            if (t.getId() == taskId) {
                taskListService.removeTask(tl, t);
                taskRepository.delete(t);
                return;
            }
        }
        log.warn("Remove task to list [ {} ] -  Task does not belong to the user", listId);
        throw new TodoListException(ErrorCode.INSUFFICIENT_PERMISSION);
    }

    public Task editTask(int userId, TaskDto dto) throws TodoListException {
        log.info("Edi task to list [ {} ]", dto.getListId());
        if (!userService.isListBelongToUser(userId, dto.getListId())) {
            log.error("Edit task to list [ {} ] -  List does not belong to the user [ {} ]", dto.getListId(), userId);
            throw new TodoListException(ErrorCode.INSUFFICIENT_PERMISSION);
        }
        try {
            Task task = taskRepository.findById(dto.getId()).orElseThrow(() -> new TodoListException(ErrorCode.UNKNOWN_TASK));

            task.setChecked(dto.isChecked());
            task.setDescription(dto.getDescription());
            task.setTitle(dto.getTitle());
            task.setLastUpdate(getCurrentDate());
            task = taskRepository.save(task);

            log.info("Edi task to list [ {} ] - Edited", dto.getListId());
            return task;
        } catch (TodoListException e) {
            throw e;
        } catch (Exception e) {
            log.error("Edit task list [ {} ] for user [ {} ] - Unknown error", dto.getId(), userId, e);
            throw new TodoListException(ErrorCode.INTERNAL_ERROR);
        }
    }

    private Date getCurrentDate() {
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }
}
