package list.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TaskListShort {
    private Integer id;

    private String title;

    private String color;

    private Integer nbTask;

    private Integer nbCompletedTask;
}
