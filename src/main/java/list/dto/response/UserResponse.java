package list.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserResponse {
    private Integer id;

    private String login;

    private String mail;

    private String name;

    private Boolean showDescription;

    private List<TaskListShort> taskLists;
}
