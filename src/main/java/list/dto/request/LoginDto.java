package list.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginDto {

    @NotBlank
    private String login;

    @NotBlank
    private String password;
}
