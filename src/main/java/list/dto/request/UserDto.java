package list.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    @NotNull
    @Size(max = 40)
    private String mail;

    @NotNull
    @Size(max = 30)
    private String name;


    @Nullable
    @Size(max = 30)
    private String password;

}
