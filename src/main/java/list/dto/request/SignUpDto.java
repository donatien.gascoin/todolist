package list.dto.request;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class SignUpDto {
    @NotBlank
    @Size(min = 1, max = 30)
    private String name;

    @NotBlank
    @Size(min = 1, max = 20)
    private String login;

    @NotBlank
    @Size(max = 40)
    @Email
    private String mail;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;
}
