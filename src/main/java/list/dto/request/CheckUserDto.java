package list.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CheckUserDto {
    private String mail;
    private String login;
}
