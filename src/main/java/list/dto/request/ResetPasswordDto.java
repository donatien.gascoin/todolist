package list.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResetPasswordDto {
    private String mail;
    private String login;
    private String password;
    private String token;
}
