package list.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto {

    @Nullable
    private int id;

    @NotNull
    private int listId;

    @NotNull
    @Size(max = 40)
    private String title;

    @Nullable
    @Size(max = 1000)
    private String description;

    @NotNull
    private boolean checked;
}
