package list.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskListDto {

    @Nullable
    private int id;

    @NotNull
    @Size(max = 40)
    private String title;

    @NotNull
    @Size(max = 10)
    private String color;

}
