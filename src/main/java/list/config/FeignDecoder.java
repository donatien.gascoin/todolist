package list.config;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import feign.Response;
import feign.codec.ErrorDecoder;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

@Slf4j
public class FeignDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == HttpStatus.BAD_REQUEST.value()) {
            try {
                String errorResponse = convertResponseToString(response);
                return new TodoListException(ErrorCode.FEIGN_ERROR, Arrays.asList(errorResponse));

            } catch (IOException ioexception) {
                return ioexception;
            }

        } else {
            return new TodoListException(ErrorCode.INTERNAL_ERROR, Arrays.asList(response.reason()));
        }
    }

    private String convertResponseToString(Response response) throws IOException {
        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return response.body().asInputStream();
            }
        };
        return byteSource.asCharSource(Charsets.UTF_8).read();
    }
}
