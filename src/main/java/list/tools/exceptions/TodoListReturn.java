package list.tools.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import list.tools.enums.ErrorCode;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TodoListReturn {

    private ErrorCode errorCode;

    private List<String> fieldInError;

    public TodoListReturn (ErrorCode e) {
        this.errorCode = e;
        fieldInError = null;
    }
}
