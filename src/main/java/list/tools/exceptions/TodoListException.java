package list.tools.exceptions;

import list.tools.enums.ErrorCode;

import java.util.List;

public class TodoListException extends Exception {

    private ErrorCode errorCode;

    private List<String> fieldInError;

    public TodoListException(ErrorCode _errorCode) {
        super();
        this.errorCode = _errorCode;
    }

    public TodoListException(ErrorCode _errorCode, List<String> _fieldInError) {
        super();
        this.errorCode = _errorCode;
        this.fieldInError = _fieldInError;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getErrorCodeLabel() {
        return errorCode.toString();
    }

    public List<String> getFieldInError() {
        return fieldInError;
    }

    public TodoListReturn returnMessage() {
        return new TodoListReturn(errorCode, fieldInError);
    }
}
