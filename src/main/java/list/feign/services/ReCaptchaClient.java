package list.feign.services;

import list.config.FeignConfig;
import list.feign.dto.CaptchaResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "todo-list", url = "${google.reCaptcha.url}", configuration =
    FeignConfig.class)
public interface ReCaptchaClient {

    @GetMapping(value = "/siteverify?secret={secret}&response={response}&remoteip={ip}")
    CaptchaResponse verify(@PathVariable("secret") String secret, @PathVariable("response") String response,
        @PathVariable("ip") String ip);
}
