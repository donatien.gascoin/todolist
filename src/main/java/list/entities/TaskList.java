package list.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "lists")
@NoArgsConstructor
@AllArgsConstructor
public class TaskList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "TITLE", length = 40)
    private String title;

    @Column(name = "COLOR", length = 10)
    private String color;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "list", orphanRemoval = true)
    private java.util.List<Task> tasks;

    @ManyToOne
    @JoinColumn(name="USER_ID", nullable=false)
    @JsonIgnore
    private User user;

    public TaskList(String title, String color, User user) {
        this.title = title;
        this.color = color;
        this.user = user;
    }
}
