package list.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tasks")
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "TITLE", length = 40)
    private String title;

    @Column(name = "DESCRIPTION", length = 1000)
    private String description;

    @Column(name = "LAST_UPDATE")
    private Date lastUpdate;

    @Column(name = "CHECKED")
    private boolean checked;

    @ManyToOne
    @JoinColumn(name="LIST_ID", nullable=false)
    @JsonIgnore
    private TaskList list;

    public Task (String title, String description, Date lastUpdate, boolean checked, TaskList taskList) {
        this.title = title;
        this.description = description;
        this.lastUpdate = lastUpdate;
        this.checked = checked;
        this.list = taskList;
    }
}
