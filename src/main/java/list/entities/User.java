package list.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "LOGIN", nullable = false, unique = true, length = 20)
    private String login;

    @Column(name = "MAIL", nullable = false, length = 40)
    private String mail;

    @Column(name = "NAME", nullable = false, length = 30)
    private String name;

    @JsonIgnore
    @Column(name = "PASSWORD", nullable = false, length = 100)
    private String password;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", orphanRemoval = true)
    private List<TaskList> taskLists;

    @Column(name = "SHOW_DESCRIPTION")
    private Boolean showDescription;

    public User(String login, String mail, String name, String password) {
        this.login = login;
        this.mail = mail;
        this.name = name;
        this.password = password;
        this.showDescription = false;
    }
}
