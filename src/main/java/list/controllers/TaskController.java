package list.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import list.dto.request.TaskDto;
import list.security.CurrentUser;
import list.security.UserPrincipal;
import list.services.TaskService;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;
import list.tools.exceptions.TodoListReturn;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @PostMapping(path = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addTask(@CurrentUser UserPrincipal user, @Valid @RequestBody TaskDto dto) {
        try {
            return ResponseEntity.ok().body(taskService.addTask(user.getId(), dto));
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @PutMapping(path = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editTask(@CurrentUser UserPrincipal user, @Valid @RequestBody TaskDto dto) {
        try {
            return ResponseEntity.ok().body(taskService.editTask(user.getId(), dto));
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @DeleteMapping(path = "/remove/{listId}/{taskId}")
    public ResponseEntity<?> removeTask(@CurrentUser UserPrincipal user, @PathVariable(name = "listId") int listId, @PathVariable(name = "taskId") int taskId) {
        try {
            taskService.removeTask(user.getId(), listId, taskId);
            return ResponseEntity.ok().build();
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }
}
