package list.controllers;

import list.dto.request.CheckUserDto;
import list.dto.request.ResetPasswordDto;
import list.security.CurrentUser;
import list.security.UserPrincipal;
import list.services.UserService;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;
import list.tools.exceptions.TodoListReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserById(@CurrentUser UserPrincipal user) {
        try {
            return ResponseEntity.ok().body(userService.getShortUser(user.getId()));
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @PostMapping(path = "/check", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserByLoginAndMail(@RequestBody CheckUserDto userDto) {
        try {
            userService.isUserByLoginAndMailExist(userDto);
            return ResponseEntity.ok().build();
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @GetMapping(path = "/description", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateDescription(@CurrentUser UserPrincipal user,
                                               @RequestParam(name = "show") Boolean show) {
        try {
            userService.updateDescription(user.getId(), show);
            return ResponseEntity.ok().build();
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @PutMapping(path = "/resetPassword", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> resetPassword(@RequestBody ResetPasswordDto dto) {
        try {
            userService.resetPassword(dto);
            return ResponseEntity.ok().build();
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }
}
