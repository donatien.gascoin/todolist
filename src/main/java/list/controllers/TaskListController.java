package list.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import list.dto.request.TaskListDto;
import list.security.CurrentUser;
import list.security.UserPrincipal;
import list.services.TaskListService;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;
import list.tools.exceptions.TodoListReturn;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/list")
public class TaskListController {

    @Autowired
    private TaskListService taskListService;

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTaskListById(@CurrentUser UserPrincipal user, @PathVariable(name = "id") int id) {
        try {
            return ResponseEntity.ok().body(taskListService.getTaskListById(user.getId(), id));
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @PostMapping(path = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addTaskList(@CurrentUser UserPrincipal user, @Valid @RequestBody TaskListDto dto) {
        try {
            return ResponseEntity.ok().body(taskListService.addTaskList(user.getId(), dto));
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @PutMapping(path = "/edit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editTaskList(@CurrentUser UserPrincipal user, @Valid @RequestBody TaskListDto dto) {
        try {
            return ResponseEntity.ok().body(taskListService.editTaskList(user.getId(), dto));
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }

    @DeleteMapping(path = "/remove/{id}")
    public ResponseEntity<?> removeTaskList(@CurrentUser UserPrincipal user, @PathVariable(name = "id") int id) {
        try {
            taskListService.removeTaskList(user.getId(), id);
            return ResponseEntity.ok().build();
        } catch (TodoListException e) {
            if (ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.returnMessage());
            }
            return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
        }
    }
}
