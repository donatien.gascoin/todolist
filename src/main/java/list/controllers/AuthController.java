package list.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import list.dto.request.LoginDto;
import list.dto.request.SignUpDto;
import list.dto.response.JwtAuthenticationResponse;
import list.security.JwtTokenProvider;
import list.services.UserService;
import list.tools.enums.ErrorCode;
import list.tools.exceptions.TodoListException;
import list.tools.exceptions.TodoListReturn;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    UserService service;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDto loginDto) {
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginDto.getLogin(),
                            loginDto.getPassword()
                    )
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = tokenProvider.generateToken(authentication);
            return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
        } catch (BadCredentialsException e) {
            log.warn("Bad credential for user [ {} ]", loginDto.getLogin());
            return ResponseEntity.badRequest().body(new TodoListReturn(ErrorCode.BAD_CREDENTIALS));
        }

    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpDto signUpDto) {

        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(service.signUpUser(signUpDto));
        } catch (TodoListException e) {
            if (!ErrorCode.INTERNAL_ERROR.equals(e.getErrorCode())) {
                return ResponseEntity.badRequest().body(new TodoListReturn(e.getErrorCode()));
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
