# Todo List

The goal is to create list, and task.

Todo list is an application based on:

- [Spring](https://spring.io)
- [VueJs](https://vuejs.org/)
- Java 8

**Default user:**

- Login: test
- Password: pwd123

## How to

### Jar application

Run command:
```
java -jar TodoList-1.0-SNAPSHOT.jar
```

Access the application by taping "localhost:80" on your favorite browser.

### War application

Add the war TodoList-1.0-SNAPSHOT.war to you Tomcat server

## Sources

You will find the backend part on this repository.

The frontend is [here](https://gitlab.com/donatien.gascoin/todolistview)

## List of framework

- Backend
  - Spring Boot for endpoint management
  - Spring Security for authentication
  - Hibernate
  - Hsqldb as database
  - Liquibase
  - Lombok

- Frontend
  - Vuetify - Framework based on Vuejs and Material Design

## Architecture

Based on a MVC framework, the application only present CRUD methods.
Users are authenticated by a json web token.

![MVC](mvc-model.jpg)


I decided to use a backend for frontend architecture.
I think this architecture easily allow you to create a CRUD application.

It also facilitate the creation multiple frontend (Android/IOS).

I used Vuetify for the frontend, because it's a light framework, easy to learn, which allow to create app quickly.

## Thing to add/improve

- Security:

Currently, all request are accepted (in order to let the js file work).
It's better to deny antything execpt the front pages

- Testing

I've only tested the entity layer. So either the services and controllers need to be tested as well.

- Split front-end and back-end

The goal was to add only 1 war which contain everything.
But, with the technologies choosen, it's time consumming, and not really effective.

- Implement a real database
- Hibernate: Collection are currently set to EAGER mode. To improve performance, use a LAZY system would be better.
- Create acount on the application
- GDPR compliance: Allow to edit and remove user acount
- 

## What's next ?

Usefull features which could be implemented
- (view) Task/List sorting (by checked, by last update)
- Add degree of importance for each task 
- Ordering the task manually (drag and drop)
- Allow to share list of task with other user (read or read/write)
- ...

