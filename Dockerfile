FROM openjdk:8

ENV TZ=CET

VOLUME /logs

COPY target/*.jar app.jar

CMD ["java", "-jar", "/app.jar"]